import java.util.Scanner;

class Calculation {
    //metric calculator function
    public static double metric(double weight, double height){
        return weight/((height/100)*(height/100));
    }

    //imperial calculator function
    public static double imperial(double weight, double height){
        return (weight/(height*height))*703;
    }

    //converts the bmi number to result
    public static String result(double a) {
        if(a<18){ return "Underweight";
        }else if(a<24){ return "Healthy";
        }else if(a<29){ return "Overweight";
        }else if(a<39){ return "Obese";
        }else{ return "Extremely obese";
        }
    }

}


public class Main {
    public static void main(String [] args){

        //input from the user
        Scanner weightIn = new Scanner(System.in);
        System.out.println("Enter your weight:");
        double userWeight = weightIn.nextDouble();
        
        Scanner heightIn = new Scanner(System.in);
        System.out.println("Enter your height:");
        double userHeight = heightIn.nextDouble();

        Calculation test = new Calculation();
        double a = test.metric(userWeight,userHeight);
        System.out.println(test.result(a));
    }

}
