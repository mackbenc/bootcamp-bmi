import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class CalculationTest {
    @Test
    void metric() {
        assertEquals(20.428357113217785, Calculation.metric(64,177));
    }

    @Test
    void imperial() {
        assertEquals(12.11891891891892, Calculation.imperial(5.9,18.5));
    }

    @Test
    void result() {
        assertEquals("Healthy", Calculation.result(22));
        assertEquals("Extremely obese", Calculation.result(43));
        assertEquals("Underweight", Calculation.result(16));
    }

}